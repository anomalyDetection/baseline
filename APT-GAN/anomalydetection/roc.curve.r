roc.curve <- function(violator,  ground.truth, list.objects,imgfile){
  
  ##EVAL
  #tp=list()
  #fp=list()
  #tn=list()
  #fn=list()
  #sensitivity=list()
  TPR=list()
  #specificity = list()
  FPR=list()
  
  
  for(cut.off in 1:length(violator) ){
    
    violator.to.consider =violator[1:cut.off]
    True.Positives=  length(intersect(violator.to.consider ,ground.truth))
    False.Positives=length(violator.to.consider)-True.Positives
    False.Negatives = length(ground.truth)-True.Positives
    True.Negatives= length(list.objects)- length(ground.truth)#length(violator)
    #length(PositivesList) - length(violator)
    
    # tp=c(tp,True.Positives)
    #  fp=c(fp,False.Positives)
    #  tn=c(tn,True.Negatives)
    #  fn=c(fn,False.Negatives)
    #sens=True.Positives/(True.Positives+False.Negatives)
    #sensitivity=c(sensitivity,sens)
    tpr=True.Positives/(True.Positives+False.Negatives)
    TPR=c(TPR,tpr)
    
    #  spec=True.Negatives/(True.Negatives+False.Positives)
    #spec=False.Positives/(True.Negatives+False.Positives)
    #specificity=c(specificity,spec)
    fpr=False.Positives/(True.Negatives+False.Positives)
    FPR=c(FPR,fpr)
    
  }
  
  
  
  TruePositiveRate = TPR
  #specificity=lapply(specificity, function(x) x=1-x)
  FalsePositiveRate =  FPR
  FalsePositiveRate=normalize.vector(unlist(FalsePositiveRate))
  pdf(imgfile) 
  ROCCurve = data.frame(cbind(FalsePositiveRate, TruePositiveRate))
  plot(ROCCurve, main="ROC Curve")
  lines(ROCCurve, col="red")
  dev.off()
  require(pROC)
  auc=roc(unlist(FalsePositiveRate),unlist(TruePositiveRate))
  return(auc$auc)
  #ROCsdat <- data.frame(cutpoint = c(-Inf, 5, 7, 9, Inf), TPR = c(0, 0.56, 0.78, 0.91, 1), FPR = c(0, 0.01, 0.19, 0.58, 1)) 
  #plot(TPR ~ FPR, data = ROCsdat, xlim = c(0,1), ylim = c(0,1), type="b", pch = 25, bg = "black")
  
  #  plot(unlist(TruePositiveRate) ~ unlist(FalsePositiveRate), data = , xlim = c(0,1), ylim = c(0,1), type="b",  bg = "black")
  # text(TPR ~ FPR, data = ROCsdat, pos = 3, labels = ROCsdat$cutpoint)
  #abline(0, 1, col="lightgrey")
  
  #library(pracma)
  #auc=trapz(unlist(FalsePositiveRate), unlist(TruePositiveRate))
  #cat("AUC n ", auc)
  
  
  
  
  
}