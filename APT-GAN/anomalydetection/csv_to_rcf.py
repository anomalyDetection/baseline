
"""
######################################################################
#### IMPORTANT: BEFORE USING THIS SCRIPT DO THE FOLLOWING STEPS:
#   $>  sudo pip3 install rpy2
#   $>  R
#   R prompt>  install.packages(c("stringr","ppls","plyr","gridBase","gridExtra","grid","lattice",latticeExtra", "RcolorBrewer","ggplot2","pROC"))
##############
####python3 rare.rules.py --input "./ProcessEvent.csv" --outputScoresFile "./scores.csv" --sup 0.05 --conf 0.97
##
#######################################################################
"""
import os
import sys
import argparse
import rpy2.robjects.packages as rpackages
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr

# save original cwd and R source cwd
oldcwd = os.getcwd()
rcwd = os.path.abspath(os.path.dirname(__file__))

# R vector of strings
from rpy2.robjects.vectors import StrVector
# import R's "base" package
importr('base')
# import R's utility package
importr('utils')
# select a mirror for R packages
#chooseCRANmirror(ind=1) # select the first mirror in the list
# R package names
#packnames = ['ggplot2', 'stringr','dplyr',"gridBase","gridExtra","grid", "lattice","latticeExtra","RColorBrewer"]
# Selectively install what needs to be install.
#packages_to_inst = [x for x in packnames if not rpackages.isinstalled(x)]
#if len(packages_to_inst) > 0:
#    install_packages(StrVector(packages_to_inst))
importr('stringr')
importr('plyr')
importr("gridBase")
importr("gridExtra")
importr("grid")
importr("lattice")
importr("latticeExtra")
importr("RColorBrewer")
importr("ggplot2")
importr("ppls")
#running an embedded R
robjects.r['options'](warn=0)
os.chdir(rcwd)
robjects.r('''source('csv.to.rcf.r')''')
r_csv_to_rcf_function = robjects.globalenv['csv.to.rcf']

parser = argparse.ArgumentParser(description='Converting csv to rcf')
parser.add_argument('--input', '-i',
					help=' an input context file', required=True)
parser.add_argument('--output', '-o',
					help='an output rcf file', required=True)		
 

def get_rcf(inputfile,outputfile):
    print ("==Running Rare Association Rule Mining \n ")
    #r=robjects.r
    #r.source("call.frequent.rules.with.BandDiagrams.r")
    #r["call.rares.rules.BandDiagrams(path, groundtruth, minsup,minconf)"]
    r_csv_to_rcf_function(inputfile, outputfile)
    

if __name__ == "__main__" :
    args = parser.parse_args()
    os.chdir(oldcwd)
    print(args.input)
    input = os.path.abspath(args.input)
    print(input)
    output = os.path.abspath(args.output)
     
    os.chdir(rcwd)
    get_rcf(input,output)
