library(ggplot2)
top.Metod=c("Windows/VR-ARM/PE","BSD/VR-ARM/PE","Linux/VR-ARM/PN","Androis/VR-ARM/PE")
top.NDCG=c(0.82,0.64,0.52,0.87)
top.AUC=c(0.75,0.75,0.83,0.92)
df=data.frame(cbind(top.AUC,top.NDCG,top.Metod))

plot.new()

ggplot(df, aes(x=df$top.AUC, y=df$top.NDCG,color=df$top.Metod)) + geom_point(size=6, alpha=0.6)+xlab("Area Under Curve") + ylab("nDCG")+title("AUC vs nDCG")
