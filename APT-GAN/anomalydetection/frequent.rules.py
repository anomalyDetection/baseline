
"""
######################################################################
#### IMPORTANT: BEFORE USING THIS SCRIPT DO THE FOLLOWING STEPS:
#   $>  sudo pip3 install rpy2
#   $>  R
#   R prompt>  install.packages(c("stringr","plyr","gridBase","gridExtra","grid","lattice",latticeExtra", "RcolorBrewer","ggplot2"))
#
#######################################################################
"""
import os
import sys
import argparse
import rpy2.robjects.packages as rpackages
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
# R vector of strings
from rpy2.robjects.vectors import StrVector
# import R's "base" package
# save original cwd and R source cwd
oldcwd = os.getcwd()
rcwd = os.path.abspath(os.path.dirname(__file__))

importr('base')
# import R's utility package
importr('utils')
# select a mirror for R packages
#chooseCRANmirror(ind=1) # select the first mirror in the list
# R package names
#packnames = ['ggplot2', 'stringr','dplyr',"gridBase","gridExtra","grid", "lattice","latticeExtra","RColorBrewer"]
# Selectively install what needs to be install.
#packages_to_inst = [x for x in packnames if not rpackages.isinstalled(x)]
#if len(packages_to_inst) > 0:
#    install_packages(StrVector(packages_to_inst))
importr('stringr')
importr('plyr')
importr("gridBase")
importr("gridExtra")
importr("grid")
importr("lattice") 
importr("latticeExtra")
importr("RColorBrewer")
importr("ggplot2")

#running an embedded R
robjects.r('''source('check_packages.r')''' )
robjects.r['options'](warn=0)
robjects.r['check_packages']
robjects.r('''source('frequent.rules.ad.r')''')
r_frequent_rule_mining_function = robjects.globalenv['frequent.rules.ad']

parser = argparse.ArgumentParser(description='Frequent Rule Mining')
parser.add_argument('--input', '-i',
					help=' an input context file', required=True)
parser.add_argument('--outputScoresFile', '-o',
					help='an output scoring file', required=True)		
parser.add_argument('--outputRanksFile', '-or', default='dummy.csv',
					help='an output ranking file')
parser.add_argument('--optimize', '-op', 	default=False,
					help='optimize the ranks with a Ground Truth (using nDCG scores)')					
parser.add_argument('--groundtruth', '-g', default='dummy.csv',
					help='A ground truth file')
parser.add_argument('--sup', '-s',
					help=' support ',
					default='97', required=True)
parser.add_argument('--conf', '-c',
					help=' confidence ', 
					default='97', required=True)
					
def extract_frequent_rules(inputfile, outputscores,outputranks,optimize, groundtruth, sup,conf):
    print ("Running FrequentAssociation Rule Mining \n ")
    #r=robjects.r
    #r.source("call.frequent.rules.with.BandDiagrams.r")
    #r["call.rares.rules.BandDiagrams(path, groundtruth, minsup,minconf)"]
    r_frequent_rule_mining_function(inputfile, outputscores,outputranks,optimize, groundtruth, sup,conf)
    


if __name__ == "__main__" :
    args = parser.parse_args()
    os.chdir(oldcwd)
    print(args.input)
    input = os.path.abspath(args.input)
    print(input)
    scores = os.path.abspath(args.outputScoresFile)
    ranks = os.path.abspath(args.outputRanksFile)
    os.chdir(rcwd)
    extract_frequent_rules(input,scores,ranks, args.optimize, args.groundtruth, args.sup,args.conf)
#
 #   args = parser.parse_args()
  #  extract_frequent_rules(args.path,args.groundtruth,args.sup,args.conf,args.CombineScores,args.LogScaleDraw
   # args.path = "./Adapt_Project/Database/ADM/cadets/pandex/",  # directory that contains the contexts
   # args.groundtruth = "./Adapt_Project/Database/ADM/cadets/pandex/cadets_pandex_merged.csv", # ground truth file
   # args.minsup = 0.05,  # support for rare itemsets
   # args.minconf = 97, # confidence
   # args.CombineScores  = True, # if true => include ProcessAll,
   # args.LogScaleDraw  = True # if true => draw band diagrams in log scale
   # )
#
