
"""
######################################################################
#### IMPORTANT: BEFORE USING THIS SCRIPT DO THE FOLLOWING STEPS:
#   $>  sudo pip3 install rpy2
#   $>  R
#   R prompt>  install.packages(c("stringr","ppls","plyr","gridBase","gridExtra","grid","lattice",latticeExtra", "RcolorBrewer","ggplot2","pROC"))
##############
####python3 rare.rules.py --input "./ProcessEvent.csv" --outputScoresFile "./scores.csv" --sup 0.05 --conf 0.97
##
#######################################################################
"""
import os
import sys
import argparse
import rpy2.robjects.packages as rpackages
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr

# save original cwd and R source cwd
oldcwd = os.getcwd()
rcwd = os.path.abspath(os.path.dirname(__file__))

# R vector of strings
from rpy2.robjects.vectors import StrVector
# import R's "base" package
importr('base')
# import R's utility package
importr('utils')
# select a mirror for R packages
#chooseCRANmirror(ind=1) # select the first mirror in the list
# R package names
#packnames = ['ggplot2', 'stringr','dplyr',"gridBase","gridExtra","grid", "lattice","latticeExtra","RColorBrewer"]
# Selectively install what needs to be install.
#packages_to_inst = [x for x in packnames if not rpackages.isinstalled(x)]
#if len(packages_to_inst) > 0:
#    install_packages(StrVector(packages_to_inst))
importr('stringr')
importr('plyr')
importr("gridBase")
importr("gridExtra")
importr("grid")
importr("lattice")
importr("latticeExtra")
importr("RColorBrewer")
importr("ggplot2")
importr("ppls")
#running an embedded R
robjects.r['options'](warn=0)
os.chdir(rcwd)
robjects.r('''source('rare.rules.ad.r')''')
r_rare_rule_mining_function = robjects.globalenv['rare.rules.ad']

parser = argparse.ArgumentParser(description='Rare Rule Mining')
parser.add_argument('--input', '-i',
					help=' an input context file', required=True)
parser.add_argument('--outputScoresFile', '-o',
					help='an output scoring file', required=True)		
parser.add_argument('--outputRanksFile', '-or', default='dummy.csv',
					help='an output ranking file')
parser.add_argument('--optimize', '-op', 	default=False,
					help='optimize the ranks with a Ground Truth (using nDCG scores)')					
parser.add_argument('--groundtruth', '-g', default='dummy.csv',
					help='A ground truth file')
parser.add_argument('--sup', '-s',
					help=' support ',
					default='0.05', required=True)
parser.add_argument('--conf', '-c',
					help=' confidence ',
					default='97', required=True)
 

def extract_rare_rules(inputfile, outputscores,outputranks,optimize, groundtruth, sup,conf):
    print ("==Running Rare Association Rule Mining \n ")
    #r=robjects.r
    #r.source("call.frequent.rules.with.BandDiagrams.r")
    #r["call.rares.rules.BandDiagrams(path, groundtruth, minsup,minconf)"]
    ndcg=r_rare_rule_mining_function(inputfile, outputscores,outputranks,optimize, groundtruth, sup,conf)
    return ndcg

if __name__ == "__main__" :
    args = parser.parse_args()
    os.chdir(oldcwd)
    print(args.input)
    input = os.path.abspath(args.input)
    print(input)
    scores = os.path.abspath(args.outputScoresFile)
    ranks = os.path.abspath(args.outputRanksFile)
    os.chdir(rcwd)
    extract_rare_rules(input,scores,ranks, args.optimize, args.groundtruth, args.sup,args.conf)
