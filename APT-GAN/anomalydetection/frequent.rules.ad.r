### VF-ARM very frequent association rule mining module.
## Author: Sid Ahmed Benabderrahmane, The University of Edinburgh. Oct 2018.
## This function runs the frequent association rule extraction function. It takes as input a folder containing the csv files of the contexts,
## a ground truth file for drawing the band diagrams, and two other parameters for the support and confidence.
## It exports the band diagrams and the density plot in ./img folder.
## This script can be called from the R shell as:
##        R Prompt>  source(call.frequent.rules.with.BandDiagrams.r)
##        R Prompt>  call.frequent.rules.BandDiagrams("/home/dummy/database/", "/home/dummy/database/groudntruth.csv", 90, 90)

frequent.rules.ad <- function(Input.file, ## input csv context file
                                OutputScoresFile,
                                OutputRankingFile, #exported ranking file
                                Optimize.With.Ground.Truth,
                                ground.truth.file, ## ground truth file. It is actually needed to get the best ranking on the different association rules quality measures (Lift, Conf., ...)
                                MinSup, # Support
                                MinConf # Confidence
)
{
  
  ######## Loading the libraries.
  ## Before running the different scripts it is advised to install the packages that are listed in check_packages.r.
  #source("frequent.rules.processing.r")
  source("load.csv.r")
  library(stringr) # load str_split
  library("plyr")
  library("ppls")
  
  
  
  MyList=vector("list",1)
  k=1 # idcontext
  ### list of quality measures of the association rules
  if(Optimize.With.Ground.Truth) {
    
    Measures.List=c("confidence","lift","leverage","PowerFactor","Kulczynski","Lerman","AddedValue","Cosine","ConterExampleRate","Loevinger","ImbalanceRatio",
                    "Jaccard","Klosgen", "Combination","MaxCombination")
    
  
    Listof_ndcg=vector("list",1)
    Listof_ranking_loss=vector("list",1)
    Best.Ranking.Measure=vector("list",1)
    
    best.list.of.scores=list()
    max.ndcg=0
    

    
    
  }else{
    cat("\n Ad detection without GT")                              
    
  }
  ###### validation  GT file
  
  gt_file=read.csv(ground.truth.file)
  gt_Objects=as.list(gt_file[gt_file$label=="AdmSubject::Node",]$uuid)
  gt_Objects=as.character(unlist(gt_Objects))
  
  #ndcg function
  ndcg <- function(ranks,num_gt){ 
    dcg = 0.0;
    maxdcg = 0.0;
    for (r in ranks) dcg = dcg + 1/log2(r + 1) 
    for (s in 1:(num_gt  )) maxdcg = maxdcg + 1/log2(s + 1)
    return(dcg/maxdcg)
  }
  maxPos = 0
  options(max.print =  10000000)
  epsilon = 0.00000000000001
  
  cat('\n ############### Frequent Rule Mining Anomaly Detection ######################## \n')
  
  ##rank loss is actually not used.
  mim.rank.loss=9999999
  idcontext=1
  
  ################ Load objects, attributes and binary matrix from CSV file.     
  cat('\n reading the current context file \n ',Input.file)
  ###We suppose that the RCF file exists in the same directory of the input csv file.
  ## Later we are going to add a check to test the existance of the RCF file. 
  ## If it is missing we will use the csv_to_rcf function to create it.
  ContextFileRCF=gsub("csv","rcf",Input.file)#paste0(path,ListofContexts[idcontext],".rcf",sep="")
  if(!file.exists(ContextFileRCF)){cat(" The Context RCF file does not exist, I'm trying to create it ! \n")
    source("csv.to.rcf.r")
    csv_to_rcf(Input.file,ContextFileRCF)
  }
  
  
  
  
  st=format(Sys.time(), "%Y-%m-%d_%H-%M")
  st=paste("_",st, "_", sep = "") 
   
  start_time <- Sys.time()
    # t=tic("FreqAsRules" )
    SoftAssRulescmd = paste0(getwd(),"/coron-0.8/core02_assrulex.sh  ",ContextFileRCF, " ", MinSup,"% ", MinConf,"% -names -alg:zart -rule:closed  -full ",sep = "") #>thisresults2.txt
    SoftAssRulesresult = try(system(SoftAssRulescmd, intern = TRUE,  wait = TRUE))     
    # tplus=toc()
    
    # cputimeList[k]=t$toc-t$tic
    #TimeAssruleslistresulttext=c(TimeAssruleslistresulttext,as.character(cputime))
    
    cat('\n Cleaning the Results of the Association Rules \n')
    
    CoronOutPut = as.list(SoftAssRulesresult)
    CoronOutPut = lapply(CoronOutPut,function(x)x[!is.na(x)])
    CoronOutPut = lapply(CoronOutPut,function(x)x[!x==""])
    CoronOutPut = Filter(length,CoronOutPut)
    CoronOutPut = CoronOutPut[14:length(CoronOutPut)-2]
    CondRules = lapply(strsplit(as.character(CoronOutPut),">"),"[",1)
    CondRules = lapply(CondRules, function(x)gsub("{", '', x, fixed = T))
    CondRules = lapply(CondRules, function(x)gsub("}", '', x, fixed = T))
    CondRules=lapply(CondRules, function(x)gsub("=", '', x, fixed = T))
    CondRules=lapply(CondRules, function(x)gsub(" ", '', x, fixed = T))
    NbRules=as.integer(length(CondRules))
    # cat("\014")  
    cat('\n Nb Extracted Association Rules',NbRules)
    
    ResRules=lapply(strsplit(as.character(CoronOutPut),">"),"[",2)
    values=lapply(strsplit(as.character(ResRules),"}"),"[",2)
    cat('\n Parsing the output \n')
    Support=lapply(strsplit(as.character(values),";"),"[",1)
    Support=lapply(strsplit(as.character(Support),"\\["),"[",2)
    Support=lapply(Support, function(x)gsub("%", '', x, fixed = T))
    Support=lapply(Support, function(x)gsub("]", '', x, fixed = T))
    Support=as.numeric(Support)
    Confidence=lapply(strsplit(as.character(values),";"),"[",2)   
    Confidence=lapply(strsplit(as.character(Confidence),"\\["),"[",2)
    Confidence=lapply(strsplit(as.character(Confidence),"%"),"[",1)
    Confidence=as.numeric(Confidence)
    #Confidence=lapply(Confidence, function(x)gsub("%", '', x, fixed = T))
    #Confidence=lapply(Confidence, function(x)gsub("\\]", '', x, fixed = T))
    ResRules=lapply(strsplit(as.character(ResRules),"}"),"[",1)
    ResRules=lapply(ResRules, function(x)gsub("{", '', x, fixed = T))
    ResRules=lapply(ResRules, function(x)gsub(" ", '', x, fixed = T))
    
    Lift=lapply(strsplit(as.character(CoronOutPut),">"),"[",2)
    Lift=lapply(strsplit(as.character(Lift),";"),"[",5)
    Lift=lapply(strsplit(as.character(Lift),"="),"[",2)
    Lift=as.numeric(Lift)
    
    
    SuppLeft=lapply(strsplit(as.character(CoronOutPut),">"),"[",2)
    SuppLeft=lapply(strsplit(as.character(SuppLeft),";"),"[",3)
    SuppLeft=lapply(strsplit(as.character(SuppLeft),"\\["),"[",2)
    SuppLeft=lapply(SuppLeft, function(x)gsub("%", '', x, fixed = T))
    SuppLeft=lapply(SuppLeft, function(x)gsub("]", '', x, fixed = T))
    SuppLeft=as.numeric(SuppLeft)
    
    
    
    SuppRight=lapply(strsplit(as.character(CoronOutPut),">"),"[",2)
    SuppRight=lapply(strsplit(as.character(SuppRight),";"),"[",4)
    SuppRight=lapply(strsplit(as.character(SuppRight),"\\["),"[",2)
    SuppRight=lapply(SuppRight, function(x)gsub("%", '', x, fixed = T))
    SuppRight=lapply(SuppRight, function(x)gsub("]", '', x, fixed = T))
    SuppRight=as.numeric(SuppRight)
    
    
    #    Conviction=lapply(strsplit(as.character(CoronOutPut),">"),"[",2)
    #   Conviction=lapply(strsplit(as.character(Conviction),";"),"[",6)
    #  Conviction=lapply(strsplit(as.character(Conviction),"="),"[",2)
    # Conviction=as.numeric(Conviction)
    #  Conviction[is.na(Conviction)]=-1
    #  maxConv=max(Conviction)
    #  Conviction[Conviction==-1]=maxConv+1
    
    # Conviction<-normalize.vector(Conviction)
    Leverage=Support-SuppLeft*SuppRight
    PowerFactor=Support *Confidence/100
    Kulczynski=0.5*(Support /SuppLeft +Support /SuppRight )
    Lerman=(Support -SuppLeft *SuppRight )/(sqrt(SuppLeft *SuppRight ))
    AddedValue=Confidence/100-SuppRight 
    Cosine=  Support / sqrt(SuppLeft *SuppRight )
    ConterExampleRate=(Support  -1+Confidence/100)/Support 
    Loevinger=(Confidence/100-SuppRight )/(1-SuppRight )
    ImbalanceRatio=abs(SuppLeft -SuppRight )/(SuppLeft +SuppRight -Support )
    Jaccard=Support/(SuppLeft+SuppRight-Support)
    Klosgen=sqrt(Support )*(Confidence/100-SuppRight )
    
    
    
    
    
    
    Confidence=lapply(Confidence, function(x)gsub("NaN", '0', x, fixed = T))
    Confidence=lapply(Confidence, function(x){((as.double(x)-0.00000000000001)) })
    Confidence=normalize.vector(unlist(Confidence))
    
    
    Lift=lapply(Lift, function(x)gsub("NaN", '0', x, fixed = T))
    Lift=lapply(Lift, function(x)as.double(x))
    NormalizedLift=normalize.vector(unlist(Lift))
    
    
    
    Leverage =lapply(Leverage, function(x)gsub("NaN", '0', x, fixed = T))
    Leverage =lapply(Leverage , function(x)as.double(x))
    # Leverage=normalize.vector(unlist(Leverage))
    
    PowerFactor=lapply(PowerFactor, function(x)gsub("NaN", '0', x, fixed = T))
    PowerFactor=lapply(PowerFactor , function(x)as.double(x))
    #PowerFactor=normalize.vector(unlist(PowerFactor))
    
    Kulczynski=lapply(Kulczynski, function(x)gsub("NaN", '0', x, fixed = T))
    Kulczynski=lapply(Kulczynski , function(x)as.double(x))
    #Kulczynski=normalize.vector(unlist(Kulczynski))
    
    Lerman=lapply(Lerman, function(x)gsub("NaN", '0', x, fixed = T))
    Lerman=lapply(Lerman , function(x)as.double(x))
    #Lerman=normalize.vector(unlist(Lerman))
    
    AddedValue=lapply(AddedValue, function(x)gsub("NaN", '0', x, fixed = T))
    AddedValue=lapply(AddedValue , function(x)as.double(x))
    #AddedValue=normalize.vector(unlist(AddedValue))
    
    Cosine=lapply(Cosine, function(x)gsub("NaN", '0', x, fixed = T))
    Cosine=lapply(Cosine , function(x)as.double(x))
    #Cosine=normalize.vector(unlist(Cosine))
    
    ConterExampleRate=lapply(ConterExampleRate, function(x)gsub("NaN", '0', x, fixed = T))
    ConterExampleRate=lapply(ConterExampleRate , function(x)as.double(x))
    # ConterExampleRate=normalize.vector(unlist(ConterExampleRate))
    
    Loevinger=lapply(Loevinger, function(x)gsub("NaN", '0', x, fixed = T))
    Loevinger=lapply( Loevinger, function(x)as.double(x))
    #Loevinger=normalize.vector(unlist(Loevinger))
    
    ImbalanceRatio=lapply(ImbalanceRatio, function(x)gsub("NaN", '0', x, fixed = T))
    ImbalanceRatio=lapply( ImbalanceRatio, function(x)as.double(x))
    #ImbalanceRatio=normalize.vector(unlist(ImbalanceRatio))
    
    Jaccard=lapply(Jaccard, function(x)gsub("NaN", '0', x, fixed = T))
    Jaccard=lapply(Jaccard , function(x)as.double(x))
    # Jaccard=normalize.vector(unlist(Jaccard))
    
    Klosgen=lapply(Klosgen, function(x)gsub("NaN", '0', x, fixed = T))
    Klosgen=lapply(Klosgen , function(x)as.double(x))
    
    
    cat('Rules extraction done! \n')
    returns_args = load.csv(Input.file)
    List_Objects = returns_args$List_Objects
    List_Attributes = returns_args$List_Attributes
    AttributesofObject = returns_args$AttributesofObject 
    ObjectOfAttributes = returns_args$ObjectOfAttributes
    #    df.AssocRulesOutPut=data.frame()
    #   df.AssocRulesOutPut=do.call(rbind, Map(data.frame, "CondRules"=CondRules,
    #                                          "ResRules"=ResRules,"Support"=Support,
    #                                          "Confidence"=Confidence,"Lift"=Lift
    #,
    #     "Conviction"=Conviction,
    #      "Leverage"=Leverage,
    #       "RulePowerFactor"=PowerFactor,
    
    #    "Kulczynski"=Kulczynski,
    #     "Lerman"=Lerman,
    #    "AddedValue"=AddedValue,
    #   "Cosine"=Cosine,
    #            "ConterExampleRate"=ConterExampleRate,
    #  "Loevinger"=Loevinger,
    #            "ImbalanceRatio"=ImbalanceRatio,
    #           "Jaccard"=Jaccard,
    #         "Klosgen"=Klosgen
    
    
    #   ))
    
    
    
    
    TopViolatedRulesForEachObjectConfidence=""
    # TopViolatedRulesForEachObjectConviction=""
    #  TopViolatedRulesForEachObjectLeverage=""
    #    TopViolatedRulesForEachObjectConfidence=""
    #    TopViolatedRulesForEachObjectLift=""
    #   TopViolatedRulesForEachObjectPowerFactor=""
    #    TopViolatedRulesForEachObjectKulczynski=""
    #    TopViolatedRulesForEachObjectLerman=""
    #    TopViolatedRulesForEachObjectAddedValue=""
    #     TopViolatedRulesForEachObjectCosine=""
    #      TopViolatedRulesForEachObjectConterExampleRate=""
    #       TopViolatedRulesForEachObjectLoevinger=""
    #       TopViolatedRulesForEachObjectImbalanceRatio=""
    #       TopViolatedRulesForEachObjectJaccard=""
    #       TopViolatedRulesForEachObjectKlosgen=""
    
    ViolatedRulesForEachObjectConfidence=""
    #    ViolatedRulesForEachObjectLift=""
    #   ViolatedRulesForEachObjectConviction=""
    #   ViolatedRulesForEachObjectLeverage=""
    #     ViolatedRulesForEachObjectPowerFactor=""
    #      ViolatedRulesForEachObjectKulczynski=""  
    #      ViolatedRulesForEachObjectLerman=""
    #       ViolatedRulesForEachObjectAddedValue=""
    #        ViolatedRulesForEachObjectCosine=""
    #        ViolatedRulesForEachObjectConterExampleRate=""
    #      ViolatedRulesForEachObjectLoevinger=""
    #      ViolatedRulesForEachObjectImbalanceRatio=""
    #      ViolatedRulesForEachObjectJaccard=""
    #     ViolatedRulesForEachObjectKlosgen=""
    
    
    TopScoreConfidence=""
    #    TopScoreLift=""
    #    TopScoreConviction=""
    #    TopScoreLeverage=""
    #    TopScorePowerFactor=""
    #     TopScoreKulczynski=""
    #     TopScoreLerman=""
    #    TopScoreAddedValue=""
    #    TopScoreCosine=""
    #   TopScoreConterExampleRate=""
    ##   TopScoreLoevinger=""
    #    TopScoreImbalanceRatio=""
    #   TopScoreJaccard=""
    #    TopScoreKlosgen=""
    
    AVGScoresOfObjectsConfidence =""
    AVGScoresOfObjectsLift =""
    #   AVGScoresOfObjectsConviction=""
    AVGScoresOfObjectsLeverage=""
    AVGScoresOfObjectsPowerFactor=""
    AVGScoresOfObjectsKulczynski=""
    AVGScoresOfObjectsLerman=""
    AVGScoresOfObjectsAddedValue=""
    AVGScoresOfObjectsCosine=""
    AVGScoresOfObjectsConterExampleRate=""
    AVGScoresOfObjectsLoevinger=""
    AVGScoresOfObjectsImbalanceRatio=""
    AVGScoresOfObjectsJaccard=""
    AVGScoresOfObjectsKlosgen=""
    MaxLiftScore=""
    
    ViolatorObjectList=""
    CondRules=lapply(CondRules, function(x)unlist(strsplit(as.character(x),split=',')))
    ResRules=lapply(ResRules, function(x)unlist(strsplit(as.character(x),split=',')))
    
    for(Obj in 1: length(List_Objects)){
      #if(Consider_List_objects_as_GroundTruth==TRUE) if(Obj !in indx) next
      GlobalscoreConf=0.0
      GlobalscoreLift=0.0
      GlobalscoreLeverage=0.0
      #     GlobalscoreConviction=0.0
      GlobalscorePowerFactor=0.0
      GlobalscoreKulczynski=0.0
      GlobalscoreLerman=0.0
      GlobalscoreAddedValue=0.0
      GlobalscoreCosine=0.0
      GlobalscoreConterExampleRate=0.0
      GlobalscoreLoevinger=0.0
      GlobalscoreImbalanceRatio=0.0
      GlobalscoreJaccard=0.0
      GlobalscoreKlosgen=0.0
      Maxliftsc=0.0
      
      Max_ScoreConf=0.0
      #      Max_ScoreLift=0.0
      #     Max_ScoreConviction=0.0
      #      Max_ScoreLeverage=0.0
      # #         Max_ScorePowerFactor=0.0
      #         Max_ScoreLerman=0.0
      #         Max_ScoreKulczynski=0.0
      #        Max_ScoreAddedValue=0.0
      ##        Max_ScoreCosine=0.0
      #        Max_ScoreConterExampleRate=0.0
      #       Max_ScoreLoevinger=0.0
      #        Max_ScoreImbalanceRatio=0.0
      #       Max_ScoreJaccard=0.0
      #       Max_ScoreKlosgen=0.0
      
      
      Max_RuleConf=""
      #   Max_RuleLift=""
      #       Max_RuleConviction=""
      #     Max_RuleLeverage=""
      #      Max_RulePowerFactor=""
      #        Max_RuleKulczynski=""
      #         Max_RuleLerman=""
      #         Max_RuleAddedValue=""
      #          Max_RuleCosine=""
      #          Max_RuleConterExampleRate=""
      #         Max_RuleLoevinger=""
      #        Max_RuleImbalanceRatio=""
      #        Max_RuleJaccard=""
      #         Max_RuleKlosgen=""
      
      
      RuleListConf=""
      # RuleListLift=""
      #      RuleListConviction=""
      #         RuleListLeverage=""
      #         RuleListPowerFactor=""
      #         RuleListKulczynski=""
      #         RuleListLerman=""
      #         RuleListAddedValue=""
      #        RuleListCosine=""
      #         RuleListConterExampleRate=""
      #         RuleListLoevinger=""
      #        RuleListImbalanceRatio=""
      #        RuleListJaccard=""
      #        RuleListKlosgen=""
      
      
      
      nbViolatedRules=0
      cat('\n ****> Scoring Object_ID: ',Obj)
      for(Rule in 1:length(CoronOutPut)){
        RulescoreConf=0.0
        RulescoreLift=0.0
        #          RulescoreConviction=0.0
        RulescoreLeverage=0.0
        RulescorePowerFactor=0.0
        
        RulescoreKulczynski=0.0
        RulescoreLerman=0.0
        RulescoreAddedValue=0.0
        RulescoreCosine=0.0
        RulescoreConterExampleRate=0.0
        RulescoreLoevinger=0.0
        RulescoreImbalanceRatio=0.0
        RulescoreJaccard=0.0
        RulescoreKlosgen=0.0
        
        #
        if(length(intersect(AttributesofObject[[Obj]],as.list(unlist( ResRules[Rule]))) )  == 0 )
        {
          #    cat("NO RHS \n")
          #   cat(Obj)
          
          #if( length(intersect(AttributesofObject[[Obj]], as.list(unlist(Condlist[Rule]))))>0 )
          
          if( length(intersect(AttributesofObject[[Obj]], as.list(unlist(CondRules[Rule])))) >= length(unlist(CondRules[Rule])) )
            #  if( length(AttributesofObject[[Obj]]) == length( unlist(Condlist[Rule]) ) )
            #  
          {
     #   if(length(intersect(AttributesofObject[[Obj]],as.list(unlist( ResRules[Rule]))) )==0)
      #  {
       #   if( length(intersect(AttributesofObject[[Obj]], as.list(unlist(CondRules[Rule]))))>0 )
        #  {
            ##there is an anomaly =============================================  
            ##violation with score based on confidence
            nbViolatedRules=nbViolatedRules+1
            leftlength=length(unlist(str_split(CondRules[[Rule]],",")))
            rightlength=length(unlist(str_split(ResRules[[Rule]],",")))
            lengthRule=leftlength+rightlength
            
            
            RulescoreConf=lengthRule*log2(1-(as.double(Confidence[Rule])-0.00000000000001)/100.0)  #*(length(ResRules[Rule])+length(CondRules[Rule]))
            GlobalscoreConf=GlobalscoreConf+RulescoreConf
            
            #some example of the most important rules with their scores
            #we can use these rules for graph pattern matching .....
            left= paste(CondRules[[Rule]],collapse=',' )
            rigth= paste(ResRules[[Rule]],collapse=',' )
            violatedrule=paste(as.character(left),as.character(rigth),sep="=>")
            sc=paste(" Score: ",as.character(-1*RulescoreConf),sep="=")
            violatedrule=paste(violatedrule,sc,sep="")
            RuleListConf=paste(RuleListConf,violatedrule,sep = " | ")
            
            if(-1*RulescoreConf>Max_ScoreConf){
              Max_ScoreConf=-1*RulescoreConf
              Max_RuleConf=violatedrule
            }
            
            
            ##violation with score based on lift
            RulescoreLift= lengthRule*log2(1-as.double(Lift[Rule])/100)#*(length(ResRules[Rule])+length(CondRules[Rule]))
            GlobalscoreLift=GlobalscoreLift+RulescoreLift
            
            Maxliftsc=max(Maxliftsc,as.double(Lift[Rule])/100)
            #   violatedrule=paste(as.character(left),as.character(rigth),sep="=>")
            #     sc=paste(" Score: ",as.character(-1*RulescoreLift),sep="=")
            #       violatedrule=paste(violatedrule,sc,sep="")
            #     RuleListLift=paste(RuleListLift,violatedrule,sep = " | ")
            #       
            #       if(-1*RulescoreLift>Max_ScoreLift){
            #        Max_ScoreLift=-1*RulescoreLift
            #         Max_RuleLift=violatedrule
            #       }
            
            ##violation with score based on Leverage
            
            RulescoreLeverage=  RulescoreLeverage+lengthRule*log2(1-as.double(Leverage[Rule])/100)
            GlobalscoreLeverage =GlobalscoreLeverage+RulescoreLeverage
            
            RulescorePowerFactor=  RulescorePowerFactor+lengthRule*log2(1-as.double(PowerFactor[Rule])/100)
            GlobalscorePowerFactor =GlobalscorePowerFactor+RulescorePowerFactor
            
            RulescoreKulczynski=  RulescoreKulczynski+lengthRule*log2(1-as.double(Kulczynski[Rule])/100)
            GlobalscoreKulczynski =GlobalscoreKulczynski+RulescoreKulczynski
            
            RulescoreLerman =  RulescoreLerman + lengthRule*log2(1-as.double(Lerman[Rule])/100)
            GlobalscoreLerman =GlobalscoreLerman + RulescoreLerman
            
            RulescoreAddedValue=  RulescoreAddedValue + lengthRule*log2(1-as.double(AddedValue[Rule])/100)
            GlobalscoreAddedValue =GlobalscoreAddedValue +RulescoreAddedValue
            
            RulescoreCosine=  RulescoreCosine +lengthRule*log2(1-as.double(Cosine[Rule])/100)
            GlobalscoreCosine =GlobalscoreCosine +RulescoreCosine
            
            RulescoreConterExampleRate =  RulescoreConterExampleRate+lengthRule*log2(1-as.double(ConterExampleRate[Rule])/100)
            GlobalscoreConterExampleRate =GlobalscoreLeverage+RulescoreConterExampleRate
            
            RulescoreLoevinger =  RulescoreLoevinger +lengthRule*log2(1-as.double(Loevinger[Rule])/100)
            GlobalscoreLoevinger = GlobalscoreLoevinger +RulescoreLoevinger
            
            RulescoreImbalanceRatio =  RulescoreImbalanceRatio + lengthRule*log2(1-as.double(ImbalanceRatio[Rule])/100)
            GlobalscoreImbalanceRatio = GlobalscoreImbalanceRatio + RulescoreImbalanceRatio
            
            RulescoreJaccard =  RulescoreJaccard + lengthRule*log2(1-as.double(Jaccard[Rule])/100)
            GlobalscoreJaccard = GlobalscoreJaccard + RulescoreJaccard
            
            RulescoreKlosgen =  RulescoreKlosgen + lengthRule*log2(1-as.double(Klosgen[Rule])/100)
            GlobalscoreKlosgen = GlobalscoreKlosgen + RulescoreKlosgen
            
            
            
            
            
            
            
            
          }
        }###================================================endif violation
        
      }##for rules
      if(nbViolatedRules>0)
      {
        cat("This object is an anomaly, and violates ",nbViolatedRules," frequent rules \n")
        GlobalscoreConf=(0-GlobalscoreConf)/nbViolatedRules
        AVGScoresOfObjectsConfidence=c(AVGScoresOfObjectsConfidence,GlobalscoreConf)
        TopViolatedRulesForEachObjectConfidence=c(TopViolatedRulesForEachObjectConfidence,Max_RuleConf)
        TopScoreConfidence=c(TopScoreConfidence,Max_ScoreConf)
        ViolatedRulesForEachObjectConfidence=c(ViolatedRulesForEachObjectConfidence,RuleListConf)
        
        GlobalscoreLift=(0-GlobalscoreLift)/nbViolatedRules
        AVGScoresOfObjectsLift=c(AVGScoresOfObjectsLift,GlobalscoreLift)
        
        MaxLiftScore=c(MaxLiftScore,Maxliftsc)
        # TopViolatedRulesForEachObjectLift=c(TopViolatedRulesForEachObjectLift,Max_RuleLift)
        #TopScoreLift=c(TopScoreLift,Max_ScoreLift)
        #ViolatedRulesForEachObjectLift=c(ViolatedRulesForEachObjectLift,RuleListLift)
        
        GlobalscoreLeverage = (0-GlobalscoreLeverage)/nbViolatedRules
        AVGScoresOfObjectsLeverage = c(AVGScoresOfObjectsLeverage,GlobalscoreLeverage)
        
        GlobalscorePowerFactor=(0-GlobalscorePowerFactor)/nbViolatedRules
        AVGScoresOfObjectsPowerFactor=c(AVGScoresOfObjectsPowerFactor,GlobalscorePowerFactor)
        
        GlobalscoreKulczynski=(0-GlobalscoreKulczynski)/nbViolatedRules
        AVGScoresOfObjectsKulczynski=c(AVGScoresOfObjectsKulczynski,GlobalscoreKulczynski)
        
        GlobalscoreLerman=(0-GlobalscoreLerman)/nbViolatedRules
        AVGScoresOfObjectsLerman=c(AVGScoresOfObjectsLerman,GlobalscoreLerman)
        
        GlobalscoreAddedValue=(0-GlobalscoreAddedValue)/nbViolatedRules
        AVGScoresOfObjectsAddedValue=c(AVGScoresOfObjectsAddedValue,GlobalscoreAddedValue)
        
        GlobalscoreCosine=(0-GlobalscoreCosine)/nbViolatedRules
        AVGScoresOfObjectsCosine=c(AVGScoresOfObjectsCosine,GlobalscoreCosine)
        
        GlobalscoreConterExampleRate=(0-GlobalscoreConterExampleRate)/nbViolatedRules
        AVGScoresOfObjectsConterExampleRate=c(AVGScoresOfObjectsConterExampleRate,GlobalscoreConterExampleRate)
        
        GlobalscoreLoevinger=(0-GlobalscoreLoevinger)/nbViolatedRules
        AVGScoresOfObjectsLoevinger=c(AVGScoresOfObjectsLoevinger,GlobalscoreLoevinger)
        
        GlobalscoreImbalanceRatio=(0-GlobalscoreImbalanceRatio)/nbViolatedRules
        AVGScoresOfObjectsImbalanceRatio=c(AVGScoresOfObjectsImbalanceRatio,GlobalscoreImbalanceRatio)
        
        GlobalscoreJaccard=(0-GlobalscoreJaccard)/nbViolatedRules
        AVGScoresOfObjectsJaccard=c(AVGScoresOfObjectsJaccard,GlobalscoreJaccard)
        
        GlobalscoreKlosgen=(0-GlobalscoreKlosgen)/nbViolatedRules
        AVGScoresOfObjectsKlosgen=c(AVGScoresOfObjectsKlosgen,GlobalscoreKlosgen)
        
        
        
        ViolatorObjectList=c(ViolatorObjectList,List_Objects[[Obj]])
        
        
      }else{
        cat("This object does not violate the frequent rules \n")
        
      }
      
    }#for obj
    
    
    
    AVGScoresOfObjectsLeverage=AVGScoresOfObjectsLeverage[-1]
    AVGScoresOfObjectsPowerFactor=AVGScoresOfObjectsPowerFactor[-1]
    AVGScoresOfObjectsKulczynski=AVGScoresOfObjectsKulczynski[-1]
    AVGScoresOfObjectsLerman=AVGScoresOfObjectsLerman[-1]
    AVGScoresOfObjectsAddedValue=AVGScoresOfObjectsAddedValue[-1]
    AVGScoresOfObjectsCosine=AVGScoresOfObjectsCosine[-1]
    AVGScoresOfObjectsConterExampleRate=AVGScoresOfObjectsConterExampleRate[-1]
    AVGScoresOfObjectsLoevinger=AVGScoresOfObjectsLoevinger[-1]
    AVGScoresOfObjectsImbalanceRatio=AVGScoresOfObjectsImbalanceRatio[-1]
    AVGScoresOfObjectsJaccard=AVGScoresOfObjectsJaccard[-1]
    AVGScoresOfObjectsKlosgen=AVGScoresOfObjectsKlosgen[-1]
    AVGScoresOfObjectsLift =AVGScoresOfObjectsLift[-1]
    MaxLiftScore=MaxLiftScore[-1]
    # TopViolatedRulesForEachObjectLift=TopViolatedRulesForEachObjectLift[-1]
    #TopScoreLift=TopScoreLift[-1]
    #ViolatedRulesForEachObjectLift= ViolatedRulesForEachObjectLift[-1]
    
    AVGScoresOfObjectsConfidence=AVGScoresOfObjectsConfidence[-1]
    #    TopViolatedRulesForEachObjectConfidence=TopViolatedRulesForEachObjectConfidence[-1]
    #    TopScoreConfidence=TopScoreConfidence[-1]
    #   ViolatedRulesForEachObjectConfidence= ViolatedRulesForEachObjectConfidence[-1]
    
    
    ViolatorObjectList=ViolatorObjectList[-1]
    
    end_time <- Sys.time()
    
    
    df.ObjectsWithScores=data.frame(matrix(ncol = 15, nrow = 0))
    # colnames (df.ObjectsWithScores)=c("Objects","ViolatedRulesForEachObjectConfidence","AVGScoresOfObjectsConfidence",
    #                        "TopViolatedRulesForEachObjectConfidence","TopScoreConfidence",
    #                       "ViolatedRulesForEachObjectLift", "AVGScoresOfObjectsLift","TopViolatedRulesForEachObjectLift",
    #                      "TopScoreLift")
    
    colnames (df.ObjectsWithScores)=c("Objects",
                                      # "ViolatedRulesForEachObjectConfidence",
                                      "AVGScoresOfObjectsConfidence",
                                      #   "TopViolatedRulesForEachObjectConfidence",
                                      #  "TopScoreConfidence",
                                      #     
                                      #      "ViolatedRulesForEachObjectLift",
                                      "AVGScoresOfObjectsLift",
                                      #      "TopViolatedRulesForEachObjectLift",
                                      #      "TopScoreLift" 
                                      "MaxLiftScore",
                                      "AVGScoresOfObjectsLeverage",
                                      "AVGScoresOfObjectsPowerFactor",
                                      "AVGScoresOfObjectsKulczynski",
                                      "AVGScoresOfObjectsLerman",
                                      "AVGScoresOfObjectsAddedValue",
                                      "AVGScoresOfObjectsCosine",
                                      "AVGScoresOfObjectsConterExampleRate",
                                      "AVGScoresOfObjectsLoevinger",
                                      "AVGScoresOfObjectsImbalanceRatio",
                                      "AVGScoresOfObjectsJaccard",
                                      "AVGScoresOfObjectsKlosgen"
                                      
                                      
    )
    if(length(ViolatorObjectList)!=0){
      
      
      AVGScoresOfObjectsConfidence=lapply(as.list(AVGScoresOfObjectsConfidence), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsLift=lapply(as.list( AVGScoresOfObjectsLift), function(x)gsub("NaN", '0', x, fixed = T))
      MaxLiftScore=lapply(as.list( MaxLiftScore), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsLeverage =lapply(as.list( AVGScoresOfObjectsLeverage), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsPowerFactor=lapply(as.list(AVGScoresOfObjectsPowerFactor ), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsKulczynski=lapply(as.list( AVGScoresOfObjectsKulczynski), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsLerman=lapply(as.list( AVGScoresOfObjectsLerman), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsAddedValue=lapply(as.list( AVGScoresOfObjectsAddedValue), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsCosine=lapply(as.list( AVGScoresOfObjectsCosine), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsConterExampleRate=lapply(as.list(AVGScoresOfObjectsConterExampleRate ), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsLoevinger=lapply(as.list( AVGScoresOfObjectsLoevinger), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsImbalanceRatio=lapply(as.list(AVGScoresOfObjectsImbalanceRatio) , function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsJaccard=lapply(as.list(AVGScoresOfObjectsJaccard ), function(x)gsub("NaN", '0', x, fixed = T))
      AVGScoresOfObjectsKlosgen=lapply(as.list( AVGScoresOfObjectsKlosgen), function(x)gsub("NaN", '0', x, fixed = T))
      
      AVGScoresOfObjectsConfidence=lapply(as.list(AVGScoresOfObjectsConfidence), function(x)as.double(x))
      AVGScoresOfObjectsLift=lapply(as.list( AVGScoresOfObjectsLift), function(x)as.double(x))
      MaxLiftScore=lapply(as.list( MaxLiftScore),function(x)as.double(x))
      
      AVGScoresOfObjectsLeverage =lapply(as.list( AVGScoresOfObjectsLeverage), function(x)as.double(x))
      AVGScoresOfObjectsPowerFactor=lapply(as.list(AVGScoresOfObjectsPowerFactor ), function(x)as.double(x))
      AVGScoresOfObjectsKulczynski=lapply(as.list( AVGScoresOfObjectsKulczynski), function(x)as.double(x))
      AVGScoresOfObjectsLerman=lapply(as.list( AVGScoresOfObjectsLerman), function(x)as.double(x))
      AVGScoresOfObjectsAddedValue=lapply(as.list( AVGScoresOfObjectsAddedValue), function(x)as.double(x))
      AVGScoresOfObjectsCosine=lapply(as.list( AVGScoresOfObjectsCosine), function(x)as.double(x))
      AVGScoresOfObjectsConterExampleRate=lapply(as.list(AVGScoresOfObjectsConterExampleRate ), function(x)as.double(x))
      AVGScoresOfObjectsLoevinger=lapply(as.list( AVGScoresOfObjectsLoevinger), function(x)as.double(x))
      AVGScoresOfObjectsImbalanceRatio=lapply(as.list(AVGScoresOfObjectsImbalanceRatio) , function(x)as.double(x))
      AVGScoresOfObjectsJaccard=lapply(as.list(AVGScoresOfObjectsJaccard ), function(x)as.double(x))
      AVGScoresOfObjectsKlosgen=lapply(as.list( AVGScoresOfObjectsKlosgen), function(x)as.double(x))
      
      
      df.ObjectsWithScores=do.call(rbind, Map(data.frame,
                                              "Objects"=as.list(ViolatorObjectList),
                                              #
                                              #    "ViolatedRulesForEachObjectConfidence"=as.list(ViolatedRulesForEachObjectConfidence),
                                              "AVGScoresOfObjectsConfidence"= AVGScoresOfObjectsConfidence ,
                                              #      "TopViolatedRulesForEachObjectConfidence"= as.list(TopViolatedRulesForEachObjectConfidence),
                                              #      "TopScoreConfidence"=as.list(TopScoreConfidence),
                                              #
                                              #    "ViolatedRulesForEachObjectLift"=as.list(ViolatedRulesForEachObjectLift),
                                              "AVGScoresOfObjectsLift"=  AVGScoresOfObjectsLift ,
                                              #     "TopViolatedRulesForEachObjectLift"=as.list(TopViolatedRulesForEachObjectLift),
                                              #      "TopScoreLift"= as.list(TopScoreLift) 
                                              "MaxLiftScore"= MaxLiftScore ,
                                              "AVGScoresOfObjectsLeverage"=  AVGScoresOfObjectsLeverage ,
                                              "AVGScoresOfObjectsPowerFactor"= AVGScoresOfObjectsPowerFactor  ,
                                              "AVGScoresOfObjectsKulczynski"=  AVGScoresOfObjectsKulczynski ,
                                              "AVGScoresOfObjectsLerman"=  AVGScoresOfObjectsLerman ,
                                              "AVGScoresOfObjectsAddedValue"= AVGScoresOfObjectsAddedValue ,
                                              "AVGScoresOfObjectsCosine"=  AVGScoresOfObjectsCosine ,
                                              "AVGScoresOfObjectsConterExampleRate"= AVGScoresOfObjectsConterExampleRate ,
                                              "AVGScoresOfObjectsLoevinger"=  AVGScoresOfObjectsLoevinger ,
                                              "AVGScoresOfObjectsImbalanceRatio"= AVGScoresOfObjectsImbalanceRatio   ,
                                              "AVGScoresOfObjectsJaccard"= AVGScoresOfObjectsJaccard ,
                                              "AVGScoresOfObjectsKlosgen"=  AVGScoresOfObjectsKlosgen 
                                              
                                              
      ))
    }
    
    
    
    cat("\n Nb of detected violator objects is this context =: ",length(as.list(ViolatorObjectList)))
    cat("\n")
    
    
    idcontext=1
    if(Optimize.With.Ground.Truth){
      ########## choose for each context the best metric that gives the best rank (through ndcg)
    ########## choose for each context the best metric that gives the best rank (through ndcg)
    cat("\n Ranking with different methods started \n")
    
    for(nb.methods in 1:(length(Measures.List)-2)){
      cat("\n Ranking with method: ",Measures.List[nb.methods])
      df.ObjectsWithScores =arrange(df.ObjectsWithScores ,desc(df.ObjectsWithScores[,1 + nb.methods]) ) 
      Viol.list =as.character(unlist(df.ObjectsWithScores$Objects)  )
      True.Positives=   intersect(Viol.list ,gt_Objects)
      True.Positives.Positions= which(Viol.list %in%  as.character(unlist(True.Positives)))
      
      
     if(length(True.Positives.Positions) >= 1){
    #    
     #   for (tp in 1:length(True.Positives.Positions)) {
      #    tp.pos=True.Positives.Positions[tp]
       #   tp.score=df.ObjectsWithScores$AVGScoresOfObjectsLift[tp.pos]
     #     True.Positives.Positions[tp]=which(df.ObjectsWithScores$AVGScoresOfObjectsLift==tp.score)[1]
     #   }
        current.ndcg.value= ndcg(True.Positives.Positions,   length(gt_Objects))
        
         current.rank.loss.value=0 #ranking_loss(True.Positives.Positions, length(List_Objects))
        
        
        # we can do the weighting ndcg and rank loss
        if(current.ndcg.value>max.ndcg){  
          max.ndcg=current.ndcg.value
          Listof_ndcg[[idcontext]] = current.ndcg.value
          Listof_ranking_loss[[idcontext]] =   current.rank.loss.value
          MyList[[idcontext]] = True.Positives.Positions
          cat("\n Best Indexes of true positives ",True.Positives.Positions)
          cat("\n Best max.ndcg ",max.ndcg)
          Best.Ranking.Measure[[idcontext]]=Measures.List[nb.methods]
          ###best list of scores
          best.list.of.scores= as.list(  df.ObjectsWithScores[MyList[[idcontext]],1 + which(Measures.List == Best.Ranking.Measure[[idcontext]])] 
          )
          
        }
        
        if(maxPos<max(True.Positives.Positions)) maxPos=max(True.Positives.Positions)
        
      } else{ # if no TP detected, then MyList[[idcontext]] would be null
        MyList[[idcontext]]=integer(0)
      }#else
      cat("\n Ranking optimization finished ~~~~")
    }
    
    
    
    df.OutputObjectsWithScores=data.frame(matrix(ncol = 3, nrow = 0))
    colnames (df.OutputObjectsWithScores)=c("UUID", "Score","Rank"  )
    if(length(True.Positives.Positions)==0){
      df.OutputObjectsWithScores[nrow(df.OutputObjectsWithScores) + 1,] = list("NULL","NULL","NULL")
      
      
    }else{
      
      df.OutputObjectsWithScores=do.call(rbind, Map(data.frame,
                                                    "UUID"=as.list(as.character(unlist(True.Positives))),
                                                    "Score"=best.list.of.scores,
                                                    
                                                    "Rank"=as.list(MyList[[idcontext]])   ))
    }
    
    
    
    
    #cadetspandex.ProcessAll.avf.normattr.ranked.csv
    ##Export the results
    ########################################################################
    Viol.list =as.character(unlist(df.ObjectsWithScores$Objects)  )
    Viol.list=unique(Viol.list)
    resultsFile=gsub("csv","frequent.txt",OutputRankingFile)
    imgfile=gsub("csv","frequent.pdf",OutputRankingFile)
    source("roc.curve.r")
    auc1=roc.curve(Viol.list,gt_Objects,List_Objects,imgfile)
    source("roc.curve.2.r")
    auc2=roc.curve.2(Viol.list,gt_Objects,List_Objects) 
    
 
    ###################################################################
     sink(resultsFile)
    cat("\n *** saving freq rule mining ranking results for this context \n ")
    #cat("\n database : ",path)
    cat("\n context name : ", Input.file)
    cat("\n MinSup : ",MinSup)
    cat("\n MinConf : ",MinConf)
    cat("\n Attack objects : ", as.character(unlist(True.Positives)) )
    cat("\n Best Ranks : ",MyList[[idcontext]], " among # ",length(List_Objects), " # Objects in the context.")
    cat("\n Best nDcg : ",max.ndcg)
    cat("\n Best Ranking Measure : ", Best.Ranking.Measure[[idcontext]])
    cat("\n CPU Running Time : ",end_time - start_time)
    cat("\n AUC1  : ",auc1)
    cat("\n AUC2  : ",auc2)
    sink()
    
    }else{ #export Lift results only
      df.ObjectsWithLiftScores=data.frame(matrix(ncol = 2, nrow = 0))
      colnames (df.ObjectsWithLiftScores)=c("UUID", "Score" )
      
      df.ObjectsWithLiftScores=do.call(rbind, Map(data.frame,
                                                  "UUID"=as.list(ViolatorObjectList),
                                                  "Score"= as.list( MaxLiftScore) ))
      
      if(length(df.ObjectsWithLiftScores$UUID)==0){        df.OutputObjectsWithScores[nrow(df.OutputObjectsWithScores) + 1,] = list("NULL","NULL")
}else{
      df.ObjectsWithLiftScores = arrange(df.ObjectsWithLiftScores ,desc(df.ObjectsWithLiftScores$Score) ) 
      # print(df.ObjectsWithLiftScores)
      #cadetspandex.ProcessAll.avf.normattr.ranked.csv
      ##Export the results
      
      write.csv(df.ObjectsWithLiftScores,file=OutputScoresFile, row.names = FALSE)
}
      # sink(paste0(getwd(),"/OutputRankings/Rare_Rules_",ListofContexts[idcontext],st,".txt",sep=""))
      cat("\n *** Results saved for this context \n ")
      #cat("\n database : ",path)
      
      
      Viol.list =as.character(unlist(df.ObjectsWithLiftScores$UUID)  )
      True.Positives=   intersect(Viol.list ,gt_Objects)
      True.Positives.Positions= which(Viol.list %in%  as.character(unlist(True.Positives)))
      current.ndcg.value=0.0
      
      if(length(True.Positives.Positions) >= 1){
        current.ndcg.value= ndcg(True.Positives.Positions,   length(gt_Objects))
        current.rank.loss.value=0 #ranking_loss(True.Positives.Positions, length(List_Objects))
      } 
      
      
      df.OutputObjectsWithScores=data.frame(matrix(ncol = 3, nrow = 0))
      colnames (df.OutputObjectsWithScores)=c("UUID", "Score","Rank"  )
      if(length(True.Positives.Positions)==0){
        df.OutputObjectsWithScores[nrow(df.OutputObjectsWithScores) + 1,] = list("NULL","NULL","NULL")
        
        
      }else{
        
        df.OutputObjectsWithScores=do.call(rbind, Map(data.frame,
                                                      "UUID"=as.list(as.character(unlist(True.Positives))),
                                                      "Score"=best.list.of.scores,
                                                      
                                                      "Rank"=as.list( True.Positives.Positions)   ))
      }
      
      write.csv(df.OutputObjectsWithScores,file=OutputRankingFile, row.names = FALSE)
      resultsFile=gsub("csv","maxlift.freq.txt",OutputRankingFile)
      sink(resultsFile)
      cat("\n *** saving freq rule mining ranking results with MAX LIFT for this context \n ")
      #cat("\n database : ",path)
      cat("\n context name : ", Input.file)
      cat("\n MinSup : ",MinSup)
      cat("\n MinConf : ",MinConf)
      cat("\n Attack objects : ", as.character(unlist(True.Positives)) )
      cat("\n Best Ranks : ",True.Positives.Positions)
      cat("\n Best nDcg : ",current.ndcg.value)
      cat("\n Ranking Measure : MAX LIFT")
      cat("\n CPU Running Time : ",end_time - start_time)
      
      sink()
      
      #cat("\n database : ",path)
      
      
      
      
    } 
    
    }