join.csv <-function(csv1,csv2){
  
  library(ff)
  library(plyr)
  
  # Read in the data
  first_csv <- read.csv(csv1)
  second_csv <- read.csv(csv2)
  
  # Convert dataframes to ffdf's, while freeing up memory
  first_csv_ff <- as.ffdf(first_csv)
  rm(first_csv)
  gc()
  second_csv_ff <- as.ffdf(second_csv)
  rm(second_csv)
  gc()
  
  # Attempt to join the two ffdf's by "Id"
 # joined_csv <- join(first_csv_ff, second_csv_ff, by="Object_ID", type="full")
  merge(first_csv_ff, second_csv_ff, by="Id", all=TRUE) 
  

  library(readr)
  library(dplyr)
  path="/home/terminator2/Documents/Adapt_Project/Database/Engagement_3/trace/main/"
  #/home/terminator2/Documents/Adapt_Project/Repository/InternalCode/internal_code-master_v4/simple/trace/pandex/"
  contxt_list=c(
  "ProcessEvent",
 "ProcessExec",
 "ProcessParent",
  "ProcessNetflow"
   )
  
  for(i in 1:length(contxt_list)){
    cat("\n",i)
    A <- read.csv(paste0(path, contxt_list[i],".csv",sep=""))
    
    for(j in 1:length(contxt_list)){
      if(i==j)next 
      if(i>j)next
      cat("\n",j)
      B <- read.csv(paste0(path,contxt_list[j],".csv",sep=""))   
      mydataAB = full_join(A, B, by="Object_ID")
      mydataAB[is.na(mydataAB)] <- 0
     # mydataAB=mydataAB[,-1]
      write.csv(file=paste0(path,"Join_Outer_",contxt_list[i],"_",contxt_list[j],".csv",sep=""),mydataAB,row.names=FALSE)
 
    }
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  library(readr)
  library(dplyr)
  
  A <- read.csv("/home/terminator2/Documents/Adapt_Project/Database/Engagement_3/cadets/main/ProcessParent.csv")
  B <- read.csv("/home/terminator2/Documents/Adapt_Project/Database/Engagement_3/cadets/main/ProcessNetflow.csv")   
  C <- read.csv("/home/terminator2/Documents/Adapt_Project/Database/Engagement_3/cadets/main/ProcessExec.csv")
  D <- read.csv("/home/terminator2/Documents/Adapt_Project/Database/Engagement_3/cadets/main/ProcessEvent.csv"
                )
  E <- read.csv("/home/terminator2/Documents/Adapt_Project/Database/ADM/5dir/pandex/ProcessChild.csv")
 # f <- read.csv("/home/terminator2/Documents/Adapt_Project/Repository/InternalCode/internal_code-master_v4/simple/trace/pandex/Join_Outer_ProcessEvent_ProcessExec.csv")
  
  
  mydataAB = full_join(A, B, by="Object_ID")
  mydataCD = full_join(C,D, by="Object_ID")
  mydataEF = full_join(E,f, by="Object_ID")

  mydataABCD=full_join(mydataAB,mydataCD, by="Object_ID")
  mydataALL=mydataABCD#full_join(mydataABCD,E, by="Object_ID")
  mydataALL[is.na(mydataALL)] <- 0
  write.csv(file=paste0("/home/terminator2/Documents/Adapt_Project/Database/Engagement_3/cadets//main/ProcessAll.csv",sep=""),mydataALL,row.names=FALSE)
  
  }