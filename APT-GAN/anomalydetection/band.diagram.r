band.diagrams<-function(name_contexts,
                       # name of input contexts
                        
                        x.limit, # x-axis max value
                        ListPositions # a list of the true positives' positions
                        ){
  library(grid)
  library(ggplot2)
  library(gridBase)
  library(gridExtra)
  library(lattice)
  library(latticeExtra)
  
 
  #clean up
  plot.new()
  grid.newpage()
  # create a grid layout relatively to the number of contexts
  pushViewport(viewport(layout = grid.layout(length(name_contexts),1)))
  MaxPosition= x.limit+10
 
  
  for(i in 1:length(name_contexts)){
    
    if(i<length(name_contexts)){title=paste0(" ",sep="")}else{
      title=paste0("Positions of the attack objects ",sep="") #  (Log10)
    }
    
    
    codbare=ggplot() +
    
      geom_vline(xintercept=array(ListPositions[[i]]), color="red")+
       
      xlab(title) + ylab(name_contexts[i])+
      theme(axis.text=element_text(size=22),
            axis.title=element_text(size=22,face="bold"))+
      
      
      theme(axis.title.y = element_text(angle = 0,size=22,face="bold"),axis.text.y=element_text(size=22) )+ 
      theme(axis.text.x=element_text(size=22,face="bold"))+ scale_x_continuous(limits=c(1, MaxPosition))+
      
       pushViewport(viewport(layout.pos.row = i))
    
    
    
    
    
    print(codbare, newpage = FALSE)
    popViewport()
  }
  
  
  
  
  }