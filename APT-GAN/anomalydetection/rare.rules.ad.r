rare.rules.ad <- function(Input.file, ## input csv context file
                          OutputScoresFile,
                          OutputRankingFile, #exported ranking file
                          Optimize.With.Ground.Truth,
                          ground.truth.file, ## ground truth file. It is actually needed to get the best ranking on the different association rules quality measures (Lift, Conf., ...)
                          MinSup, # Support
                           MinConf # Confidence
                          ){
 
  source("rules.processing.r")
  source("load.csv.r")
  library(stringr) # load str_split
  library("plyr") 
  library("ppls")
   MyList=vector("list",1)
  ### list of quality measures of the association rules
  if(Optimize.With.Ground.Truth) {
    
  Measures.List=c("confidence","lift","leverage","PowerFactor","Kulczynski","Lerman","AddedValue","Cosine","ConterExampleRate","Loevinger","ImbalanceRatio",
                  "Jaccard","Klosgen", "Combination","MaxCombination","MaxLift")
  
  ###### validation  GT file
  

  Listof_ndcg=vector("list",1)
  Listof_ranking_loss=vector("list",1)
  Best.Ranking.Measure=vector("list",1)
  
  best.list.of.scores=list()
  max.ndcg=0
  
  #ndcg function

  
  
  }else{
    cat("\n Ad detection without GT")                              
    
                                }
  gt_file=read.csv(ground.truth.file)
  gt_Objects=as.list(gt_file[gt_file$label=="AdmSubject::Node",]$uuid)
  #gt_Objects=as.list(gt_file$uuid)
  gt_Objects=as.character(unlist(gt_Objects))
  ndcg <- function(ranks,num_gt){ 
    dcg = 0.0;
    maxdcg = 0.0;
    for (r in ranks) dcg = dcg + 1/log2(r + 1) 
    for (s in 1:(num_gt)) maxdcg = maxdcg + 1/log2(s + 1)
    return(dcg/maxdcg)
  }
  
  maxPos = 0
  options(max.print =  10000000)
  epsilon = 0.00000000000001
  
  cat('\n ############### Rare Rule Mining Anomaly Detection ######################## \n')

  ##rank loss is actually not used.
  mim.rank.loss=9999999
  idcontext=1
   
  ################ Load objects, attributes and binary matrix from CSV file.     
   cat('\n reading the current context file \n ',Input.file)
  ###We suppose that the RCF file exists in the same directory of the input csv file.
  ## Later we are going to add a check to test the existance of the RCF file. 
  ## If it is missing we will use the csv_to_rcf function to create it.
  ContextFileRCF=gsub("csv","rcf",Input.file)#paste0(path,ListofContexts[idcontext],".rcf",sep="")
  if(!file.exists(ContextFileRCF)){cat("**** The Context RCF file does not exist, I'm trying to create it ! \n")
  #  source("csv.to.rcf.r")
    csv.to.rcf(Input.file,ContextFileRCF)
    }
  
  
  start_time <- Sys.time()
  
  cat("\n starting the extraction of the association rules \n")
  RareAssRulescmd=paste0(getwd(),"/coron-0.8/core02_assrulex.sh  ",ContextFileRCF, " ", MinSup,"% "," 97% -names -alg:BtB -rule:rare  -full  -examples",sep="")  #-full -examples",sep="")
  RareAssRulesresult=try(system(RareAssRulescmd, intern = TRUE,  wait = TRUE)) 
  #toc()   ./start.sh sample/laszlo.rcf 3 -names -alg:apriorirare -nonzero
  #if(DisplayFull)capture.output(RareAssRulesresult,file=paste0("./contexts/Rare_AssociationRules_Conf_",currentview,"_",MinConf,"_Sup_",MinSup,".txt",sep=""))
  #####################
  rules.processing.results = rules.processing(RareAssRulesresult)
  cat('\n Rules extraction done! \n')
  
  returns_args = load.csv(Input.file)
  List_Objects = returns_args$List_Objects
  List_Attributes = returns_args$List_Attributes
  AttributesofObject = returns_args$AttributesofObject 
  ObjectOfAttributes = returns_args$ObjectOfAttributes
  
  ###########=========================================================================================
  ListScoreConf <- list()
  ListScoreLift <- list()
  ListscoreLeverage <- list()
  ListscorePowerFactor <- list()
  ListscoreKulczynski <- list()
  ListscoreLerman <- list()
  ListscoreAddedValue <- list()
  ListscoreCosine <- list()
  ListscoreConterExampleRate <- list()
  ListscoreLoevinger <- list()
  ListscoreImbalanceRatio <- list()
  ListscoreJaccard <- list()
  ListscoreKlosgen <- list()
  ListscoreCombination <- list()
  MaxListscoreCombination <- list()
  MaxLift <- list()
  
  
  ##reading the results
  PositivesList=rules.processing.results$Positives.List #
  RulesList=rules.processing.results$Rules.List
  objectsOfRules=rules.processing.results$objectsOf.Rules
  CondRules=rules.processing.results$Cond.Rules
  ResRules=rules.processing.results$Res.Rules
  Confidence =rules.processing.results$Confidence.sc
  Lift =rules.processing.results$Lift.sc
  Leverage =rules.processing.results$Leverage.sc
  PowerFactor =rules.processing.results$PowerFactor.sc
  Kulczynski =rules.processing.results$Kulczynski.sc
  Lerman  =rules.processing.results$Lerman.sc
  AddedValue =rules.processing.results$AddedValue.sc
  Cosine =rules.processing.results$Cosine.sc
  ConterExampleRate =rules.processing.results$ConterExampleRate.sc
  Loevinger =rules.processing.results$Loevinger.sc
  ImbalanceRatio =rules.processing.results$ImbalanceRatio.sc
  Jaccard =rules.processing.results$Jaccard.sc
  Klosgen=rules.processing.results$Klosgen.sc
  #Combination=rules.processing.results$Combination.sc
  #  MaxCombination=rules.processing.results$MaxCombination.sc
  
  ##testing the toxicity of each object && scoring the objects.
  for (id in 1:length(PositivesList)){
    cat("\n Scoring Object_ID", id, " : ", PositivesList[id],"\n")
    CurrentObject=PositivesList[id]
    scoreConf=0.0
    scoreLift=0.0
    scoreLeverage=  0.0
    scorePowerFactor= 0.0
    scoreKulczynski= 0.0
    scoreLerman= 0.0
    scoreAddedValue=  0.0
    scoreCosine=  0.0
    scoreConterExampleRate=0.0 
    scoreLoevinger= 0.0
    scoreImbalanceRatio= 0.0
    scoreJaccard= 0.0
    maxliftscore = 0.0
    scoreKlosgen= 0.0
    # scoreCombination=0.0
    #MaxscoreCombination=0.0
    
    
    
    NbValidRules=0
    ## Check the toxicity of the current object % each rule.
    for (idRule in 1:length(RulesList)){
      #cat("\n Rule \n",idRule)
      if(CurrentObject %in% objectsOfRules[[idRule]]){
        NbValidRules=NbValidRules+1
        leftlength=length(unlist(str_split(CondRules[[idRule]],",")))
        rightlength=length(unlist(str_split(ResRules[[idRule]],",")))
        lengthRule=leftlength+rightlength
        scoreConf=scoreConf+lengthRule*log2(1-(as.double(Confidence[[idRule]]-epsilon) )  )  #log2(1-as.double(Confidence[Rule])/100)
        scoreLift=scoreLift+lengthRule*log2(1-as.double(Lift[[idRule]] ))
        maxliftscore=max(maxliftscore,as.double(Lift[[idRule]]) )
        scoreLeverage=  scoreLeverage+lengthRule*log2(1-as.double(Leverage[[idRule]]) )
        scorePowerFactor= scorePowerFactor+lengthRule*log2(1-as.double(PowerFactor[[idRule]]))
        scoreKulczynski= scoreKulczynski+lengthRule*log2(1-as.double(Kulczynski[[idRule]]))
        scoreLerman= scoreLerman+lengthRule*log2(1-as.double(Lerman[[idRule]]))
        scoreAddedValue= scoreAddedValue+lengthRule*log2(1-as.double(AddedValue[[idRule]]))
        scoreCosine=  scoreCosine+lengthRule*log2(1-as.double(Cosine[[idRule]]))
        scoreConterExampleRate=scoreConterExampleRate+lengthRule*log2(1-as.double(ConterExampleRate[[idRule]]))
        scoreLoevinger= scoreLoevinger+lengthRule*log2(1-as.double(Loevinger[[idRule]]))
        scoreImbalanceRatio= scoreImbalanceRatio+lengthRule*log2(1-as.double(ImbalanceRatio[[idRule]]))
        scoreJaccard=scoreJaccard+lengthRule*log2(1-as.double(Jaccard[[idRule]]))
        scoreKlosgen= scoreKlosgen+lengthRule*log2(1-as.double(Klosgen[[idRule]]))
        #       scoreCombination=scoreCombination+lengthRule*log2(1-as.double(Combination[[idRule]]))
        #      MaxscoreCombination=MaxscoreCombination+lengthRule*log2(1-as.double(MaxCombination[[idRule]]))
        
      }
      
      
    }
    
    if(NbValidRules!=0){
      scoreConf=(0-scoreConf)/NbValidRules
      scoreLift=(0-scoreLift)/NbValidRules
      
      scoreLeverage=(0-scoreLeverage)/NbValidRules
      scorePowerFactor=(0-scorePowerFactor)/NbValidRules
      scoreKulczynski=(0-scoreKulczynski)/NbValidRules
      scoreLerman=(0-scoreLerman)/NbValidRules
      scoreAddedValue=(0-scoreAddedValue)/NbValidRules
      scoreCosine=(0-scoreCosine)/NbValidRules
      scoreConterExampleRate=(0-scoreConterExampleRate)/NbValidRules
      scoreLoevinger=(0-scoreLoevinger)/NbValidRules
      scoreImbalanceRatio=(0-scoreImbalanceRatio)/NbValidRules
      scoreJaccard=(0-scoreJaccard)/NbValidRules
      scoreKlosgen=(0-scoreKlosgen)/NbValidRules
      #    scoreCombination=(0-scoreCombination)/NbValidRules
      #   MaxscoreCombination=(0-MaxscoreCombination)/NbValidRules
      
      
      
    }
    ListScoreConf=c(ListScoreConf,scoreConf)
    ListScoreLift=c(ListScoreLift,scoreLift*100)
    ListscoreLeverage <- c( ListscoreLeverage ,  scoreLeverage )
    ListscorePowerFactor <-  c(  ListscorePowerFactor, scorePowerFactor )
    ListscoreKulczynski <-  c( ListscoreKulczynski , scoreKulczynski )
    ListscoreLerman <-   c( ListscoreLerman , scoreLerman  )
    ListscoreAddedValue <-  c(ListscoreAddedValue  , scoreAddedValue )
    ListscoreCosine <-  c(ListscoreCosine  , scoreCosine  )
    ListscoreConterExampleRate <-  c( ListscoreConterExampleRate , scoreConterExampleRate )
    ListscoreLoevinger <-  c( ListscoreLoevinger ,  scoreLoevinger )
    ListscoreImbalanceRatio <-  c(  ListscoreImbalanceRatio, scoreImbalanceRatio  )
    ListscoreJaccard <- c(  ListscoreJaccard,  scoreJaccard )
    ListscoreKlosgen <-  c( ListscoreKlosgen , scoreKlosgen  )
    MaxLift <- c( MaxLift,maxliftscore)
  }
  ListscoreCombination <-rowMeans(cbind(unlist(ListScoreConf), unlist(ListScoreLift),unlist(ListscoreLeverage), unlist(ListscorePowerFactor), unlist(ListscoreKulczynski),
                                        unlist(ListscoreLerman), unlist(ListscoreAddedValue), unlist(ListscoreCosine), unlist(ListscoreConterExampleRate),unlist(ListscoreLoevinger),
                                        unlist(ListscoreImbalanceRatio), unlist(ListscoreJaccard), unlist(ListscoreKlosgen)), na.rm=TRUE)
  
  MaxListscoreCombination <- pmax( unlist(ListScoreConf), unlist(ListScoreLift), unlist(ListscoreLeverage), unlist(ListscorePowerFactor), unlist(ListscoreKulczynski),
                                   unlist(ListscoreLerman), unlist(ListscoreAddedValue), unlist(ListscoreCosine), unlist(ListscoreConterExampleRate), unlist(ListscoreLoevinger),
                                   unlist(ListscoreImbalanceRatio), unlist(ListscoreJaccard), unlist(ListscoreKlosgen))
  
  
  end_time <- Sys.time()
  
  df.ObjectsWithScores=data.frame(matrix(ncol = 17, nrow = 0))
  
  
  colnames (df.ObjectsWithScores)=c("Objects",
                                    "AVGScoresOfObjectsConfidence",
                                    "AVGScoresOfObjectsLift",
                                    "AVGScoreLeverage",
                                    "AVGScorePowerFactor",
                                    "AVGScoreKulczynski",
                                    "AVGScoreLerman",
                                    "AVGScoreAddedValue",
                                    "AVGScoreCosine",
                                    "AVGScoreConterExampleRate",
                                    "AVGScoreLoevinger",
                                    "AVGScoreImbalanceRatio",
                                    "AVGScoreJaccard",
                                    "AVGScoreKlosgen",
                                    "AVGScoreCombination",
                                    "AVGMaxScoreCombination",
                                    "AVGMaxLift"
                                    
                                    
  )
  
  
  ListScoreConf=lapply(ListScoreConf, function(x)gsub("NaN", '0', x, fixed = T))
  ListScoreConf=lapply(ListScoreConf, function(x)as.double(x))
  
  MaxLift=lapply(MaxLift, function(x)gsub("NaN", '0', x, fixed = T))
  MaxLift=lapply(MaxLift, function(x)as.double(x))
  
  ListScoreLift=lapply(ListScoreLift, function(x)gsub("NaN", '0', x, fixed = T))
  ListScoreLift=lapply(ListScoreLift, function(x)as.double(x))
  ListscoreLeverage =lapply(ListscoreLeverage, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreLeverage =lapply(ListscoreLeverage , function(x)as.double(x))
  ListscorePowerFactor=lapply(ListscorePowerFactor, function(x)gsub("NaN", '0', x, fixed = T))
  ListscorePowerFactor=lapply(ListscorePowerFactor , function(x)as.double(x))
  ListscoreKulczynski=lapply(ListscoreKulczynski, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreKulczynski=lapply(ListscoreKulczynski , function(x)as.double(x))
  ListscoreLerman=lapply(ListscoreLerman, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreLerman=lapply(ListscoreLerman , function(x)as.double(x))
  ListscoreAddedValue=lapply(ListscoreAddedValue, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreAddedValue=lapply(ListscoreAddedValue , function(x)as.double(x))
  ListscoreCosine=lapply(ListscoreCosine, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreCosine=lapply(ListscoreCosine , function(x)as.double(x))
  ListscoreConterExampleRate=lapply(ListscoreConterExampleRate, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreConterExampleRate=lapply(ListscoreConterExampleRate , function(x)as.double(x))
  ListscoreLoevinger=lapply(ListscoreLoevinger, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreLoevinger=lapply( ListscoreLoevinger, function(x)as.double(x))
  ListscoreImbalanceRatio=lapply(ListscoreImbalanceRatio, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreImbalanceRatio=lapply( ListscoreImbalanceRatio, function(x)as.double(x))
  ListscoreJaccard=lapply(ListscoreJaccard, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreJaccard=lapply(ListscoreJaccard , function(x)as.double(x))
  ListscoreKlosgen=lapply(ListscoreKlosgen, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreKlosgen=lapply(ListscoreKlosgen , function(x)as.double(x))
  ListscoreCombination=lapply(ListscoreCombination, function(x)gsub("NaN", '0', x, fixed = T))
  ListscoreCombination=lapply(ListscoreCombination , function(x)as.double(x))
  MaxListscoreCombination=lapply(MaxListscoreCombination, function(x)gsub("NaN", '0', x, fixed = T))
  MaxListscoreCombination=lapply(MaxListscoreCombination , function(x)as.double(x))
  
  
  
  df.ObjectsWithScores=do.call(rbind, Map(data.frame,
                                          "Objects"=as.list(PositivesList),
                                          "AVGScoresOfObjectsConfidence"=ListScoreConf,
                                          "AVGScoresOfObjectsLift"=ListScoreLift,
                                          "AVGScoreLeverage" = ListscoreLeverage ,
                                          "AVGScorePowerFactor" =ListscorePowerFactor,
                                          "AVGScoreKulczynski" =ListscoreKulczynski,
                                          "AVGScoreLerman" =ListscoreLerman,
                                          "AVGScoreAddedValue" =ListscoreAddedValue,
                                          "AVGScoreCosine" =ListscoreCosine,
                                          "AVGScoreConterExampleRate" =ListscoreConterExampleRate,
                                          "AVGScoreLoevinger" = ListscoreLoevinger,
                                          "AVGScoreImbalanceRatio" =ListscoreImbalanceRatio,
                                          "AVGScoreJaccard" =ListscoreJaccard,
                                          "AVGScoreKlosgen"  = ListscoreKlosgen,
                                          "AVGScoreCombination"=ListscoreCombination,
                                          "AVGMaxScoreCombination"=MaxListscoreCombination,
                                          "AVGMaxLift"=MaxLift
                                          
  ))
  
  
 
  
  if(Optimize.With.Ground.Truth){
  ########## choose for each context the best metric that gives the best rank (through ndcg)
  cat("\n Ranking with different methods started \n")
  
  for(nb.methods in 1:(length(Measures.List))){
    cat("\n Ranking with method: ",Measures.List[nb.methods])
    df.ObjectsWithScores =arrange(df.ObjectsWithScores ,desc(df.ObjectsWithScores[,1 + nb.methods]) ) 
    Viol.list =as.character(unlist(df.ObjectsWithScores$Objects)  )
    Viol.list=unique(Viol.list)
    True.Positives=   intersect(Viol.list ,gt_Objects)
    True.Positives.Positions= which(Viol.list %in%  as.character(unlist(True.Positives)))
    
    
    if(length(True.Positives.Positions) >= 1){
       current.rank.loss.value=0 #ranking_loss(True.Positives.Positions, length(List_Objects))
      
      
    #     for (tp in 1:length(True.Positives.Positions)) {
     #     tp.pos=True.Positives.Positions[tp]
    #      tp.score=df.ObjectsWithScores$AVGScoresOfObjectsLift[tp.pos]
    #      True.Positives.Positions[tp]=which(df.ObjectsWithScores$AVGScoresOfObjectsLift==tp.score)[1]
     #   }
        current.ndcg.value= ndcg(True.Positives.Positions,   length(gt_Objects))
       
      
      
      
      # we can do the weighting ndcg and rank loss
      if(current.ndcg.value>max.ndcg){  
        max.ndcg=current.ndcg.value
        Listof_ndcg[[idcontext]] = current.ndcg.value
        Listof_ranking_loss[[idcontext]] =   current.rank.loss.value
        MyList[[idcontext]] = True.Positives.Positions
        cat("\n Best Indexes of true positives ",True.Positives.Positions)
        cat("\n Best max.ndcg ",max.ndcg)
        Best.Ranking.Measure[[idcontext]]=Measures.List[nb.methods]
        ###best list of scores
        best.list.of.scores= as.list(  df.ObjectsWithScores[MyList[[idcontext]],1 + which(Measures.List == Best.Ranking.Measure[[idcontext]])] 
        )
        
        source("band.diagram.r")
        ListPositions=MyList 
        
        band.diagrams("contexts",      length(ListPositions)+5, ListPositions   )
      }
      
      if(maxPos<max(True.Positives.Positions)) maxPos=max(True.Positives.Positions)
      
    } else{ # if no TP detected, then MyList[[idcontext]] would be null
      MyList[[idcontext]]=integer(0)
    }#else
    cat("\n Ranking optimization finished ~~~~")
  }
  
  
  
  df.OutputObjectsWithScores=data.frame(matrix(ncol = 3, nrow = 0))
  colnames (df.OutputObjectsWithScores)=c("UUID", "Score","Rank"  )
  if(length(True.Positives.Positions)==0){
    df.OutputObjectsWithScores[nrow(df.OutputObjectsWithScores) + 1,] = list("NULL","NULL","NULL")
    
    
  }else{
    
    df.OutputObjectsWithScores=do.call(rbind, Map(data.frame,
                                                  "UUID"=as.list(as.character(unlist(True.Positives))),
                                                  "Score"=best.list.of.scores,
                                                  
                                                  "Rank"=as.list(MyList[[idcontext]])   ))
  }
  
  
  
  
  #cadetspandex.ProcessAll.avf.normattr.ranked.csv
  ##Export the results
  ########################################################################
  Viol.list =as.character(unlist(df.ObjectsWithScores$Objects)  )
  Viol.list=unique(Viol.list)
  write.csv(df.OutputObjectsWithScores,file=OutputRankingFile, row.names = FALSE)
 
  write.csv(Viol.list,file=OutputScoresFile, row.names = FALSE)
  resultsFile=gsub("csv","rare.txt",OutputRankingFile)
  imgfile=gsub("csv","rare.pdf",OutputRankingFile)
  source("roc.curve.r")
  auc1 = roc.curve(Viol.list,gt_Objects,List_Objects,imgfile)
  source("roc.curve.2.r")
  auc2 = roc.curve.2(Viol.list,gt_Objects,List_Objects) 
  ###################################################################
  
  sink(resultsFile)
  cat("\n *** saving rare rule mining ranking results for this context \n ")
  #cat("\n database : ",path)
  cat("\n context name : ", Input.file)
  cat("\n MinSup : ",MinSup)
  #cat("\n MinConf : ",MinConf)
  cat("\n Attack objects : ", as.character(unlist(True.Positives)) )
  cat("\n Best Ranks : ",MyList[[idcontext]], " among # ",length(List_Objects), " # Objects in the context.")
  cat("\n Best nDcg : ",max.ndcg)
  cat("\n Best Ranking Measure : ", Best.Ranking.Measure[[idcontext]])
  cat("\n CPU Running Time : ",end_time - start_time)
  cat("\n AUC1  : ",auc1)
  cat("\n AUC2  : ",auc2)
   sink()
   
return(max.ndcg)
  
  }
  else{ #export Lift results only **********************************************************************************
    df.ObjectsWithLiftScores=data.frame(matrix(ncol = 2, nrow = 0))
    colnames (df.ObjectsWithLiftScores)=c("UUID", "Score" )
    
    df.ObjectsWithLiftScores=do.call(rbind, Map(data.frame,
                                                         "UUID"=as.list(PositivesList),
                                                         "Score"= MaxLift ))
    
    df.ObjectsWithLiftScores = arrange(df.ObjectsWithLiftScores ,desc(df.ObjectsWithLiftScores$Score) ) 
    write.csv(df.ObjectsWithLiftScores,file=OutputScoresFile, row.names = FALSE)
    # sink(paste0(getwd(),"/OutputRankings/Rare_Rules_",ListofContexts[idcontext],st,".txt",sep=""))
    cat("\n *** Results saved for this context \n ")
    # print(df.ObjectsWithLiftScores)
    #cadetspandex.ProcessAll.avf.normattr.ranked.csv
    ##Export the results
    Viol.list =as.character(unlist(df.ObjectsWithLiftScores$UUID)  )
    True.Positives=   intersect(Viol.list ,gt_Objects)
    True.Positives.Positions= which(Viol.list %in%  as.character(unlist(True.Positives)))
    current.ndcg.value=0.0
    
    if(length(True.Positives.Positions) >= 1){
      current.ndcg.value= ndcg(True.Positives.Positions,   length(gt_Objects))
      current.rank.loss.value=0 #ranking_loss(True.Positives.Positions, length(List_Objects))
    } 
      
    
    df.OutputObjectsWithScores=data.frame(matrix(ncol = 3, nrow = 0))
    colnames (df.OutputObjectsWithScores)=c("UUID", "Score","Rank"  )
    if(length(True.Positives.Positions)==0){
      df.OutputObjectsWithScores[nrow(df.OutputObjectsWithScores) + 1,] = list("NULL","NULL","NULL")
      
      
    }else{
      
      df.OutputObjectsWithScores=do.call(rbind, Map(data.frame,
                                                    "UUID"=as.list(as.character(unlist(True.Positives))),
                                                    "Score"=best.list.of.scores,
                                                    
                                                    "Rank"=as.list( True.Positives.Positions)   ))
    }
    
    write.csv(df.OutputObjectsWithScores,file=OutputRankingFile, row.names = FALSE)
    resultsFile=gsub("csv","maxlift.rare.txt",OutputRankingFile)
    sink(resultsFile)
    cat("\n *** saving rare rule mining ranking results with MAX LIFT for this context \n ")
    #cat("\n database : ",path)
    cat("\n context name : ", Input.file)
    cat("\n MinSup : ",MinSup)
    #cat("\n MinConf : ",MinConf)
    cat("\n Attack objects : ", as.character(unlist(True.Positives)) )
    cat("\n Best Ranks : ",True.Positives.Positions)
    cat("\n Best nDcg : ",current.ndcg.value)
    cat("\n Ranking Measure : MAX LIFT")
    cat("\n CPU Running Time : ",end_time - start_time)
    
    sink()
   
    #cat("\n database : ",path)
     
    
    
  }
  
  
  
}







csv.to.rcf<-function(csv_file,Out_put_rcf_file){
  
  
  cat("==== csv to rcf \n")
  if(is.null(Out_put_rcf_file))stop("error in rcf_context_file file   - terminating")
  #if(is.null(csv_file))stop("error in csv_file file   - terminating")
  OutCon<-file(Out_put_rcf_file,"w")
  
  
  cat('\n Init System LOAD FROM  CSV... \n ') 
  cat('\n Parameters are: \n ')
  cat('\n CSV \n ',csv_file)
  cat('\n rcf \n ',Out_put_rcf_file)
  
  source("load.csv.r")
  returns_args = load.csv(csv_file)
  List_Objects=returns_args$List_Objects
  List_Attributes=returns_args$List_Attributes
  AttributesofObject=returns_args$AttributesofObject 
  ObjectOfAttributes = returns_args$ObjectOfAttributes
  
  cat("List_Objects \n",length(List_Objects))
  cat("List_Attributes \n",length(List_Attributes))
  ##################################### generating the RCF file
  cat('\n Start creating RCF Context \n ')
  
  writeLines("[Relational Context]", OutCon)
  writeLines("Default Name", OutCon)
  writeLines("[Binary Relation]", OutCon)
  writeLines("Name_of_dataset", OutCon)
  writeLines(paste(List_Objects, collapse = ' | '),OutCon)
  writeLines(paste(List_Attributes, collapse = ' | '),OutCon)
  for(i in 1:length(List_Objects)){
    matching_List <-  ifelse(List_Attributes %in% AttributesofObject[[i]], 1, 0)
    writeLines( paste(matching_List, collapse = ' ') ,OutCon)
    
  }
  
  writeLines("[END Relational Context]", OutCon)
  cat('\n  RCF Context successfully created \n ')
  ##################################### generating the csv file 
  close(OutCon)
}
